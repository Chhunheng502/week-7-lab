# Freelancer Project

Welcome to Freelancer project

## Local setup

* Clone project from remote
```
git clone git@gitlab.com:ng-paragon-2021/section1/week-7-lab.git
```

__Note__: Place project inside `htdocs` folder

* Start your local developer stack

* Visit the site with `localhost:PORT\week-7-lab`

__Note__: replace `PORT` with your actual port number